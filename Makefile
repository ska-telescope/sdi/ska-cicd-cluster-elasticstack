INVENTORY_FILE ?= ./inventory_elasticstack
LOGGING_INVENTORY_FILE ?= ./inventory_logging
PRIVATE_VARS ?= ./elasticstack_vars.yml
NODES ?= logging
EXTRA_VARS ?=
V ?=
COLLECTIONS_PATHS ?= ./collections

.PHONY: vars help reinstall install
.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

ANSIBLE_COLLECTIONS_PATHS := $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
STACK_CLUSTER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/stack_cluster/playbooks
DOCKER_PLAYBOOKS := $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "LOGGING_INVENTORY_FILE=$(LOGGING_INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"

uninstall:  # uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/* $(COLLECTIONS_PATHS)/.collected

install:  ## Install dependent ansible collections
	if [ -f $(COLLECTIONS_PATHS)/.collected ]; then \
	echo "Allready collected !"; \
	else \
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections; \
	ansible-galaxy install -r requirements.yml --roles-path ./roles; \
	touch $(COLLECTIONS_PATHS)/.collected; \
	fi

reinstall: uninstall install ## reinstall collections

all: build

check_production:
	@grep 'production: true' $(PRIVATE_VARS) || (printf "aborting, as this is not production\n"; exit 1)

build_nodes: check_production install## Build nodes based on heat-cluster
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
	--extra-vars="inventory_file=../../../../$(INVENTORY_FILE)"
	make build_common
	make build_docker

build_common:  ## apply the common roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
					  -e @$(PRIVATE_VARS) \
					  --extra-vars="$(EXTRA_VARS)"

build_docker:  ## apply the docker roles
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook  -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml \
					  -e @$(PRIVATE_VARS) \
					  --extra-vars="$(EXTRA_VARS)"

clean_nodes:
	[ ! -e $(PRIVATE_VARS) ] || touch $(PRIVATE_VARS)
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook -e @$(PRIVATE_VARS) \
					 --extra-vars="$(EXTRA_VARS)" \
					 -i $(STACK_CLUSTER_PLAYBOOKS)/inventory \
					 $(STACK_CLUSTER_PLAYBOOKS)/remove-cluster-infra.yml

clean: clean_nodes  ## destroy the nodes - CAUTION THIS DELETES EVERYTHING!!!

check_nodes: check_production ## Check nodes based on heat-cluster
	ansible -i ./$(INVENTORY_FILE) cluster \
	                         -m shell -a 'df; lsblk'

lint: install ## Lint check playbook
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint --offline playbooks/stack.yml playbooks/logging.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/cluster-infra.yml \
				 $(STACK_CLUSTER_PLAYBOOKS)/common.yml \
				 $(DOCKER_PLAYBOOKS)/docker.yml

build_stack:  ## Build elasticstack
	ansible-playbook -i $(INVENTORY_FILE) playbooks/stack.yml \
	-e @$(PRIVATE_VARS) --extra-vars="$(EXTRA_VARS)"
	make build_logging NODES=elkmasters
	
build_elk_haproxy:
	ansible-playbook -i $(INVENTORY_FILE) -l $(NODES) playbooks/loadbalancers.yml \
	-e @$(PRIVATE_VARS) $(V)

stack: build_stack

build_logging:  ## Install all logging
	ansible-playbook -i $(LOGGING_INVENTORY_FILE) -l $(NODES) playbooks/logging.yml \
	-e @$(PRIVATE_VARS) $(V)

logging: build_logging

filebeat:  ## Install filebeat
	ansible-playbook -i $(LOGGING_INVENTORY_FILE) -l $(NODES) playbooks/logging.yml \
	-e @$(PRIVATE_VARS) --tags filebeat

rotate:  ## Install rotate
	ansible-playbook -i $(LOGGING_INVENTORY_FILE) -l $(NODES) playbooks/logging.yml \
	-e @$(PRIVATE_VARS) --tags rotate

clean_stack:  ## Remove elasticstack containers and stack-data
	ansible-playbook -i $(INVENTORY_FILE) playbooks/unstack.yml \
	-e @$(PRIVATE_VARS)

build: build_nodes build_stack  ## Build nodes and elasticstack

check: ## Health check
	ip=`grep 'master-0' $(INVENTORY_FILE) | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	curl "http://$${ip}:9200/_cluster/health/?wait_for_status=yellow&timeout=50s&pretty"

nodecheck: ## Node state
	ip=`grep 'master-0' $(INVENTORY_FILE) | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	curl "http://$${ip}:9200/_nodes/process?pretty"

statecheck: ## Whole cluster state
	ip=`grep 'master-0' $(INVENTORY_FILE) | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	curl "http://$${ip}:9200/_cluster/state?pretty"

add_skampi_index: ## Add the SKAMPI log index configuration
	ip=`grep 'master-0' $(INVENTORY_FILE) | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	curl -XPUT --retry-connrefused --connect-timeout 5 --retry 999  --retry-delay 1 "http://$${ip}:9200"/_ingest/pipeline/ska_log_parsing_pipeline -H "kbn-xsrf: true" -H "Content-Type: application/json" -d @./tools/ska_log_parsing_pipeline.json; \
	curl -XPUT --retry-connrefused --connect-timeout 5 --retry 999  --retry-delay 1 "http://$${ip}:9200"/_ilm/policy/ska_ilm_policy -H "kbn-xsrf: true" -H "Content-Type: application/json" -d @./tools/ska_ilm_policy.json ; \
	curl -XPUT --retry-connrefused --connect-timeout 5 --retry 999  --retry-delay 1 "http://$${ip}:9200"/_template/ska_index_template -H "kbn-xsrf: true" -H "Content-Type: application/json" -d @./tools/ska_index_template.json

get_skampi_index: ## View the SKAMPI log index configuration
	ip=`grep 'master-0' $(INVENTORY_FILE) | cut -d '=' -f 2 | cut -d ' ' -f 1`; \
	curl -XGET "http://$${ip}:9200"/_ingest/pipeline/ska_log_parsing_pipeline?pretty -H "kbn-xsrf: true" -H "Content-Type: application/json"

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
